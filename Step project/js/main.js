$(function () {

    const tabsTitle = $('.tabs-title');
    let arrTest = [];


    tabsTitle.on('click', function (event) {

        const clickTab = event.currentTarget;
        const removeActive = $('.tabs-title.active');
        $(removeActive).removeClass('active');
        if (clickTab) {
            $(clickTab).addClass('active');
            const activeTab = $('.active');
            const textActivated = $('.tabs-txt');

            const activeTabData = $(activeTab).data('name');


            $(textActivated).each(function () {
                const textActivatedElement = $(this).data('name');

                if (activeTabData === textActivatedElement) {
                    $(this).removeAttr('hidden');
                    $(this).css('display', 'flex');
                } else {
                    $(this).attr('hidden', 'hidden');
                    $(this).css('display', 'none');
                }

            })

        }

    });


    function mathRand() {
        return Math.floor(Math.random() * 24) + 1;
    }

    function randDataName() {
        const arrAttr = ['graphicDesign', 'webDesign', 'landingPages', 'wordpress'];
        let randAttr = Math.floor(Math.random() * arrAttr.length);
        return arrAttr[randAttr];
    }

    const buttonLoad = $('.btn_software.softwareClick');
    let count = 0;

    buttonLoad.on('click', function (e) {
        count++;
        const galleryOurMost = $('.gallery-our-most');
        let galleryLength = $('.gallery-our-most img').length;
        const animationLoad = $('.animation-load');

        animationLoad.css('display', 'block');
        buttonLoad.css('display', 'none');

        let timerLoad = setTimeout(function () {
            animationLoad.css('display', 'none');
            for (let i = 0; i < 12; i++) {
                const img = document.createElement('img');
                img.src = `img/AmazingWorkLoad/${mathRand()}.jpg`;
                img.setAttribute('data-name', randDataName());

                const wrapperAbout = document.createElement('div');
                wrapperAbout.classList.add('wrapper-about');

                const aboutSectionWork = document.createElement('div');
                aboutSectionWork.classList.add('about_section_work');

                const linkImgAbout = document.createElement('span');
                linkImgAbout.classList.add('link-img-about');

                const chainLogoGallery = document.createElement('a');
                chainLogoGallery.classList.add('chain_logo_gallery');
                chainLogoGallery.setAttribute('href', '#');

                const aLink = document.createElement('a');
                aLink.setAttribute('href', '#');

                const iFaLink = document.createElement('i');
                iFaLink.classList.add('fas', 'fa-link');

                const iFaSearch = document.createElement('i');
                iFaSearch.classList.add('fas', 'fa-search');

                const txtFirstAbout = document.createElement('span');
                txtFirstAbout.classList.add('txt_first_about');
                txtFirstAbout.innerText = 'creative design';

                const txtSecondAbout = document.createElement('span');
                txtSecondAbout.classList.add('txt_second_about');

                wrapperAbout.appendChild(aboutSectionWork);
                aboutSectionWork.appendChild(linkImgAbout);
                linkImgAbout.appendChild(chainLogoGallery);
                linkImgAbout.appendChild(aLink);
                chainLogoGallery.appendChild(iFaLink);
                aLink.appendChild(iFaSearch);
                aboutSectionWork.appendChild(txtFirstAbout);
                aboutSectionWork.appendChild(txtSecondAbout);

                arrTest.push(img);
                const activeFilter = $('.activeFilter').data('name');

                if (activeFilter === 'all') {
                    wrapperAbout.appendChild(img);
                    galleryOurMost.append($(wrapperAbout));
                } else {
                    const imgs = getList();
                    const galleryOurMost = document.getElementsByClassName('gallery-our-most')[0];
                    for (let i in imgs) {
                        let imgClass = imgs[i];
                        imgClass.classList.add('imgClass');
                        galleryOurMost.appendChild(imgClass);
                    }

                }

                if (galleryLength < 36) {
                    txtSecondAbout.innerText = img.getAttribute('data-name');
                    if (galleryLength < 24) {
                        buttonLoad.css('display', 'flex');
                    }
                } else {
                    buttonLoad.css('display', 'none');
                    clearTimeout(timerLoad);
                    break;
                }

            }

            function getList() {
                return arrTest.filter(function (i) {
                    const activeFilter = $('.activeFilter').data('name');
                    const arrData = $(i).data('name');
                    if (arrData === activeFilter) {
                        return i;
                    } else if (activeFilter === 'all') {
                        return i;
                    }
                })
            }
        }, 2000);
        e.preventDefault();

    });


    const filterImg = $('.filterImg');

    $(filterImg).on('click', function (event) {

        const clickTab = event.currentTarget;
        const removeActive = $('.filterImg.activeFilter');
        $(removeActive).removeClass('activeFilter');

        if (clickTab) {
            $(clickTab).addClass('activeFilter');
            const activeTab = $('.activeFilter');
            const textActivated = $('.wrapper-about');
            const activeTabName = $(activeTab).data('name');
            const imgClass = $('.imgClass');


            imgClass.each(function () {
                let imgList = $(this).data('name');
                if (imgList === activeTabName) {
                    $(this).removeAttr('hidden');
                } else if (activeTabName === 'all') {
                    $(this).removeAttr('hidden');
                } else if (imgList !== activeTabName) {
                    $(this).attr('hidden', 'hidden');
                }
            });

            textActivated.filter(function () {
                const elemDataName = $(this).find('img').data('name');

                if (elemDataName === activeTabName) {
                    $(this).removeAttr('hidden');

                } else if (activeTabName === 'all') {
                    $(this).removeAttr('hidden');


                } else {
                    $(this).attr('hidden', 'hidden');

                }

            })

        }

        event.preventDefault();
    });



    const arrCarouselReviews = [];


    function arrCarousel() {

        let elemCarousel = $('.border_img_preview');

        for (let i = 0; i < elemCarousel.length; i++) {
            $(elemCarousel).removeClass('border_img_preview');
            $(elemCarousel).addClass('border_img_preview_js');
            arrCarouselReviews.push(elemCarousel[i]);
        }
        return arrCarouselReviews;
    }


    function buildCarousel(arr) {
        const ElemCarouselChildren = $('.carousel_children_none');

        let imgList = arr;
        const carouselWrapper = $('.carousel_wrapper');

        for (let i = 0; i <= imgList.length; i++) {
            $(ElemCarouselChildren).append(imgList[i]);
        }

        $(carouselWrapper).append(ElemCarouselChildren);
    }


    function animationCarousel() {

        const btnLeft = $('.btn_left');
        const btnRight = $('.btn_right');
        const carouselChildren = $('.carousel_children');
        let correctArrPosition = arrCarousel();
        const addClassActive = $('.border_img_preview_js');
        const borderImgReviews = $('.border_img_reviews img');
        const currentLeftValue = 0;
        const ElemCarouselChildrenClick = $('.clickTarget');

        function classActive(elem) {
            addClassActive.each(function () {
                if (elem === this) {
                    $(this).addClass('activeCarousel');
                    $(this).find('.border_second_preview').addClass('activeCarousel');
                    const imgSrcCarousel = $(this).find('img');

                    if (imgSrcCarousel.attr('src') !== borderImgReviews.attr('src')) {
                        borderImgReviews.attr('src', imgSrcCarousel.attr('src'));
                    }
                } else {
                    $(this).removeClass('activeCarousel');
                    $(this).find('.border_second_preview').removeClass('activeCarousel');

                }
            })
        }


        btnLeft.on('click', function () {
            carouselChildren.remove();
            if (currentLeftValue === 0) {
                const elemFirst = correctArrPosition.pop();
                correctArrPosition.unshift(elemFirst);
                buildCarousel(correctArrPosition);
            }

            correctArrPosition.forEach(function (i) {
                if ($(i).hasClass('activeCarousel')) {
                    let elementArr = $(i)[0];
                    let elementIndex = correctArrPosition.indexOf($(elementArr).prev()[0]);

                    classActive(correctArrPosition[elementIndex]);
                }

            });

        });


        btnRight.on('click', function () {
            carouselChildren.remove();
            if (currentLeftValue === 0) {
                const elemFirst = correctArrPosition.shift();
                correctArrPosition.push(elemFirst);
                buildCarousel(correctArrPosition);
            }

            correctArrPosition.forEach(function (i) {
                if ($(i).hasClass('activeCarousel')) {
                    console.log('Active--->',i); /// -----> Два элема вместо одного!!!!!

                    let elementArr = $(i)[0];
                    let elementIndex = correctArrPosition.indexOf($(elementArr).next()[0]);


                    if (elementIndex === -1) {
                        classActive(correctArrPosition[0]);
                    } else if (elementIndex === 1 || elementIndex === 3) {
                        console.log(elementIndex);
                        classActive(correctArrPosition[elementIndex]);
                    } else if (elementIndex === 2) {
                        console.log(elementIndex);

                        // classActive(correctArrPosition[2]);
                    }

                }
            });
            // classActive(correctArrPosition[1]);
        });

        ElemCarouselChildrenClick.on('click', function (event) {

            const currentElem = event.target;
            const parentElem = $(currentElem).parent().parent()[0];
            classActive(parentElem);
        })


    }

    animationCarousel();


});