/* ЗАДАНИЕ - 2
* Написать функцию которая будет принимать два аргумента - число с которого НАЧАТь отсчет и число до которого нужно досчитать.
* Под отсчетом имеется в виду последовательный вывод чисел в консоль с увеличением на единицу.
*/

/* ЗАДАНИЕ - 3
* Написать функцию, которая будет суммировать ВСЕ числа, которые будут переданы ей в качестве аргументов.
* */

/* ЗАДАНИЕ - 4
* Написать СТРЕЛОЧНУЮ функцию, которая выводит переданное ей аргументом сообщение, указанное количество раз.
* Принимает два аргумента - само сообщение и число сколько раз его показать.
* Если первый аргумент(сообщение) не передан - ПО УМОЛЧАНИЮ присвоить этому аргументу - "Empty message"
* Если второй аргумент(количество раз) не передан - ПО УМОЛЧАНИЮ присвоить этому аргументу значение 1.
* */

/* ЗАДАНИЕ - 5
* Написать СТРЕЛОЧНУЮ функцию, которая возвращает максимальный переданый ей аргумент.
* Т.е. функции может быть передано потенциально бесконечное количество аргументов(чисел),
* вернуть нужно самый большой из них.
* */

/* ЗАДАНИЕ - 6
* Переписать калькулятор следующим образом:
*
* За каждую операцию будет отвечать отдельная функция,
* т.е. для сложения - add(a,b), для умножения - multiple(a,b) и т.д.
* Каждая из них принимает в аргументы только два числа и возвращает результат операции над двумя числами
* Если число не передано в функцию аргументом - ПО УМОЛЧАНИЮ присваивать этому аргументу 0.
*
* Основная функция calculate()
* Принимает ТРИ АРГУМЕНТА:
*   1 - число
*   2 - число
*   3 - функция которую нужно выполнить для двух этих чисел.
* Таким образом получается что основная функция калькулятор будет вызывать переданную ей аргументом функцию для двух чисел, которые передаются остальными двумя аргументами.
* */


// preparing params for action
let prepareParams = () => {
    // let [ value1, value2 ] = requestNumbers(2);
    let result = requestNumbers(2);
    let value1 = result[0];
    let value2 = result[1];

    if (isNumbersInvalid(value1, value2)) {
        return  {
            error: "<p style='color:red'>Some entered number isn\'t a number</p>"
        };
    }

    return {
            value1: Number(value1),
            value2: Number(value2)
        };

};

//validation params function
const isNumbersInvalid = (a, b) => {
    return !Number(a) || !Number(b);
}

// for getting numbers from user
let requestNumbers = count => {
  let digits = [];
  if (typeof(count) !== 'number') return  digits;

    for (i=0; i<count; i++) {
        digits[i] = prompt("Enter a number");
    }

        return digits;
}

// operations to do with numbers
let plus = (a, b) => {
    return a+b;
}

let minus = (a, b) => {
    return a-b;
}

let mult = (a, b) => {
    return a*b;
}

let delit = (a, b) => {
    if (b===0) {
        return {
            error: "<p style='color:red'>You can't devide number by zero.</p>"
        };
    }
        return a/b;
};
// operations to do with numbers


const getOperation = () => {
    return confirm('Enter operation you like');
}

// final action f-ion
let finalAction = (operation, dataNumbers) => {
    if (dataNumbers.error) {
       return document.write(dataNumbers.error);
    }
    // const {value1, value2} = dataNumbers;
    const value1 = dataNumbers.value1;
    const value2 = dataNumbers.value2;

    return operation(value1,  value2);
}

//final action execution
// let finalResult = finalAction(plus,  {value1: 15, value2: 20});
// if (finalResult != undefined) alert(finalResult);

//

let getSum = (...params) =>{
    let sum = 0;
    for (i=0; i < params.length; i++){
        sum = sum + params[i];
    }
    return sum;
}

alert(getSum(1, 7, 8, 8));
