const array = ['Ivan', 'Petro', 'Anna', 'Denis'];
const p1 = array[0];
const p2 = array[1];
const p3 = array[2];
const p4 = array[3];

const [ p1, p2, p3, p4 ] = array;

const human = {
    name: 'Ivan',
    sName: 'Ivanov',
    age: 44,
    position: 'developer'
}

const name = human.name;
const sName = human.sName;
const age = human.age;
const position = human.position;


const { name, sName, age, position, hasCar } = human;



