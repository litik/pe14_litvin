// let userChoice = +prompt('Enter start number', '888');
// const endPoint = userChoice+10;
//
// console.log(userChoice);
//
// while (userChoice !== endPoint) {
//     userChoice++;
//     console.log(userChoice);
// }

// for(let startPoint = 555; startPoint <= 630; startPoint += 5) {
//     console.log(startPoint);
// }

// const obj = {
//     name: 'Gogi',
//     age: 55,
//     pets: []
// };
//
// for (let gygy in obj) {
//     console.log(gygy);
//     // console.log(obj[gygy]);
// }

// const array = [1,2,3,4,33,[],{},'sfdsdf'];
// for (let i = 0; i < array.length; i++) {
//     console.log(array[i]);
// }
// for (let element of array) {
//     console.log(element);
// }

/* ЗАДАНИЕ - 1
* Пользователь должен ввести Имя и свой возраст.
* В случае если он вводит не соответствующие данные, нужно переспрашивать его,
* ДО ТЕХ ПОР ПОКА данные не будут введены корректно.
* Не корректными данными считается: число при вводе имени и буквенные символы при вводе возраста
* */
// let name = prompt('Enter a name');
// let parsedName = parseInt(name);
//
//
// while(isFinite(parsedName)){
//     console.log(name);
//     name = prompt('Wrong name');
//     parsedName = parseInt(name);
// }
// while(!isNaN(parsedName)){
//     console.log(name);
//     name = prompt('Wrong name');
//     parsedName = parseInt(name);
// }
//
// let age = prompt("Enter your age");
// let parsedAge = parseInt(age);
//
// while(isNaN(parsedAge)){
//     console.log(age);
//     age = prompt("Wrong age");
//     parsedAge = parseInt(age);
// }

/* ЗАДАНИЕ - 2
* Вывести в консоль первые 147 непарных чисел.
*   ПРОДВИНУТАЯ СЛОЖНОСТЬ - не выводить в консоль те из них, которые делятся на 5.
* */
// for(let start=1; start<294; start+=2){
//     if(start%5 !== 0){
//         console.log(start)
//     }
// }



/* ЗАДАНИЕ - 3
* Осуществляем проверку на корректность введения данных.
* Пользователь должен ввести два числа и символ операции.
* Если пользователь ввел НЕ числа или операцию, которой нет в списке - спрашиваем все по новой, ДО ТЕХ ПОР ПОКА не введет правильно.
*  Список операций:
*   * - умножение
*   + - добавление
*   - - отнимание
*   / - деление
* */

// let firstNumber = parseFloat(prompt('Enter first number: '));
// let secondNumber = parseFloat(prompt('Enter second number: '));
//
// while (isNaN(firstNumber) || isNaN(secondNumber)) {
//     firstNumber = parseFloat(prompt('Enter first number again: '));
//     secondNumber = parseFloat(prompt('Enter second number again: '));
// }
//
// let operator = prompt('Enter operator: ');
//
// while (operator !== '+' && operator !== '-' && operator !== '/' && operator !== '*') {
//     operator = prompt('Enter operator again: ')
// }



/* ЗАДАНИЕ - 4 калькулятор
* Спрашиваем у пользователя два числа и операцию, которую над ними нужно совершить.
* Осуществляем проверку на корректность введения данных из предыдущего задания.
* Как результат выводим на экран сообщение с результатом выбранной операции над введенными числами.
*/

// switch(operator){
//     case '+':
//         // console.log("+ " + firstNumber+secondNumber);
//         console.log(`${firstNumber} + ${secondNumber} = ${firstNumber+secondNumber}`);
//         break;
//     case '-':
//         // console.log("- " + firstNumber-secondNumber);
//         console.log(`${firstNumber} - ${secondNumber} = ${firstNumber-secondNumber}`);
//         break;
//     case "/":
//         // console.log("/ " + firstNumber/secondNumber);
//         console.log(`${firstNumber} / ${secondNumber} = ${firstNumber/secondNumber}`);
//         break;
//     case "*":
//         // console.log("* " + firstNumber*secondNumber);
//         console.log(`${firstNumber} * ${secondNumber} = ${firstNumber*secondNumber}`);
//         break;
// }

/* ПРОДВИНУТАЯ СЛОЖНОСТЬ:
*     - к списку операций добавить -> возведение в степень, взятие корня числа №1 степени числа №2.
*     - хранить в памяти результат последней операции. Если вместо одного из двух чисел введено `PREV_OP` подставлять вместо него результат предыдущей операции
*     - "обернуть" код калькулятора в функцию, которая принимает три аргумента - число1, число2, операция.
*/

/* ЗАДАНИЕ - 5
* Дан объект:*/
let user = {
    name: 'Kenny',
    surname: 'Doe',
    birthDay: '01.12.1988',
    description: 'best men ever',
    pets: {
        name: 'Bob',
        age: 14,
        status: 'dead'
    }

};

/* Вывести в консоль все свойства объекта в таком формате:
* имя сойства - значение свойства.*/
for (let key in user){
    console.log(user[key]);
    console.log(key);
    // console.log(`${key} - ${user[key]}`);
}
/* 	ЗАДАНИЕ-3.1
* 	Дан массив ['Gogi', 'Goga', 'Gogo', 'Gugu', 'Gunigugu', 'Guguber', 'Gigi'].
* 	Вывести в консоль по очереди все значение которые в нем есть ДВУМЯ способами.
* 	*/











