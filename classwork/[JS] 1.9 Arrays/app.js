
/* ЗАДАЧА - 1
* Написать функцию, которая не принимает аргументов и спрашивает пользователя что он ест на завтрак
* Считается что пользователь может ввести только одно блюдо за один раз
* По этому нужно продолжать спрашивать ДО ТЕХ ПОР ПОКА ответом от пользователя не прийдет пустой ввод.
* КАЖДОЕ БЛЮДО, которое введет пользователь нужно помещать в массив fantasticBreakfast, который
* и будет возвращаемым значением функции.
* */

/* ЗАДАЧА - 2
* Написать функцию, которая будет принимать аргументом массив, в котором каждый елемент - название блюда которое пользователь ест на завтрак.
* Нужно поочередно вывести в консоль каждое блюдо из этого массива. При этом удаляя данное блюдо из массива.
* Таком образом после того как все блюда окажутся в консоли - массив должен остаться пустым
* Возвращаемое значение - отсутствует
* Сделать это при помощи классического цикла for И попробовать использовать for of
* */

/* ЗАДАЧА - 3
* Написать функцию, которая принимает в качестве аргумента массив
* Возвращаемое значение - новый, другой массив со всеми елементами того, который был передан агрументом
* Сделать это при помощи: обычного циклка for, при помощи метода массива map(), при помощи оператора spread.
*/

/* ЗАДАЧА - 4
* Написать функцию getItemList(), которая будет получать от пользователя строку с перечисленными через запятую названиями товаров
* (они могут повторяться). После этого нужно преобразовать строку в МАССИВ С УНИКАЛЬНЫМИ ЗНАЧЕНИЯМИ
*/

/* ЗАДАЧА - 5
* есть некий "склад", он же исходный массив, внутри которого лежат названия товаров.
* Пользователь желает вместо определенного товара(!одного!) вставить один или несколько новых.
*
* НУЖНО написать функцию replaceItems(insteadOf, insertValue):
* где insteadOf хранит строчное значение названия товара ВМЕСТО какого именно товара он будет вставлять новые.
* ОБЯЗАТЕЛЬНО ПРОВЕРИТЬ ЕСТЬ ЛИ ТАКОЙ ТОВАР НА СКЛАДЕ, если товара нет - спросить у пользователя корректные данные.
* insertValue - список елементов которые нужно вставить - один или несколько
*
* ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ - отсутствует
* В исходном массиве(складе) ЗАМЕНИТЬ указанный товар, НА insertValue так, чтобы длинна исходного массива изменилась, т.е. каждый введенный пользователем товар добавляется отдельным товаром в уже существующий склад.
*/


// let aa = new Array(5);
// let aaa = Array();
// arr[arr.length-2];

// a.length = 5;
// a[10] = 100;
// a.length = 2;
// alert(a[2]);
// const x = function x() {}
// let aaa = ['he', true, [], {}, null, undefined, NaN,  x];
// aaa[aaa.length-1]();

// a.push();
// a.pop();
//
// a.shift();
// a.unshift();
// let newA = a.slice(1, 2);
// console.log(newA);
// let newA = a.splice(3, 0, 3.5);
// console.log(newA);
// const aaaa = a.concat([6,7,8,9]);
// console.log(aaaa);
// a.push([6,7,8,9]);
// console.log(a);
// [1,2,3,4,5,[6,7,8,9]]


// console.log(a.join(' -- '));
// a.reverse();
// a.sort();

// a.sort((x,y) => y-x);
// a.forEach(function(i) {
//     console.log(i);
// // });
// console.log(a.some(i => i%2 !=0)); // ||
// console.log(a.every(i => i%2 == 0)); // &&



// let a = [2,4,6];
// const x = a.find(item => item == 40);
// const y = a.filter(item => item != 4);
// const z = a.map(item => item+100);
//
// const aaaaaa = a.reduce(func);
//
//
// console.log(aaaaaa);


// console.log(x); // true/ 2 /1
// console.log(y); // [true false true true true] [2,6,7,8,10]
// console.log(z); // [2,6,7,8,10]

// for (let i = 0; i<a.length; i++) {
//
// }

// for(let item of a){
//     console.log(item);
// }
//
// for (let i in a) {
//     console.log(a[i]);
// }
//


/* ЗАДАЧА - 1
* Написать функцию, которая не принимает аргументов и спрашивает пользователя что он ест на завтрак
* Считается что пользователь может ввести только одно блюдо за один раз
* По этому нужно продолжать спрашивать ДО ТЕХ ПОР ПОКА ответом от пользователя не прийдет пустой ввод.
* КАЖДОЕ БЛЮДО, которое введет пользователь нужно помещать в массив fantasticBreakfast, который
* и будет возвращаемым значением функции.
* */
// let useransver = function(){
//     let ansver= prompt('что он ест на завтрак');
//     let arr = [];
//
//     while (ansver){
//         arr.push(ansver);
//         ansver= prompt('что он ест на завтрак');
//     }
//     return arr
// }
// let res = useransver()
// console.log(res);
//
//
//
// let humanData = [{
//     data: 'dresses',
//     count: 5,
//     onePrice: 4.99
// }, {
//     data: 'skirts',
//     count: 3,
//     onePrice: 6.99
// },{
//     data: 'hats',
//     count: 12,
//     onePrice: 9.90
// },{
//     data: 'shoes',
//     count: 12,
//     onePrice: 15.59
// }]

// const calcSummProduct = function (arrayOfData) {
    // let totalSum = 0;
    // for (let i = 0; i<arrayOfData.length; i++) {
    //     if (arrayOfData[i].count >= 10) {
    //         arrayOfData[i].onePrice = arrayOfData[i].onePrice * 0.75;
    //     }
    //     const totalSumOfOneProduct = arrayOfData[i].count * arrayOfData[i].onePrice;
    //     totalSum = totalSum + totalSumOfOneProduct;
    // }
    // return totalSum;
    // const newArr = arrayOfData.map(({data, count, onePrice}) => {
    //     if (count >= 10) {
    //         onePrice = onePrice*0.75;
    //     }
    //     return {
    //         data,
    //         count,
    //         onePrice
    //     }
    // })
//     const newArr = arrayOfData.map((item) => {
//         if (item.count >= 10) {
//             item.onePrice = item.onePrice*0.75;
//         }
//
//         return {
//             ...item
//         }
//     })
//
//     return newArr.reduce((prev, next) => next.count*next.onePrice, 0);
// }
// const totalSum = calcSummProduct(humanData);
// console.log(totalSum);

// const currencies = [
//     {
//         "r030":36,"txt":"Австралійський долар","rate":16.268951,"cc":"AUD","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":124,"txt":"Канадський долар","rate":18.170532,"cc":"CAD","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":156,"txt":"Юань Женьмiньбi","rate":3.37852,"cc":"CNY","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":191,"txt":"Куна","rate":3.554736,"cc":"HRK","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":203,"txt":"Чеська крона","rate":1.01906,"cc":"CZK","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":208,"txt":"Данська крона","rate":3.527217,"cc":"DKK","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":344,"txt":"Гонконгівський долар","rate":3.071909,"cc":"HKD","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":348,"txt":"Форинт","rate":0.0784841,"cc":"HUF","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":356,"txt":"Індійська рупія","rate":0.3417032,"cc":"INR","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":360,"txt":"Рупія","rate":0.00170016,"cc":"IDR","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":364,"txt":"Іранський ріал","rate":0.0005734,"cc":"IRR","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":376,"txt":"Новий ізраїльський шекель","rate":6.912682,"cc":"ILS","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":392,"txt":"Єна","rate":0.2227777,"cc":"JPY","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":398,"txt":"Теньге","rate":0.062128,"cc":"KZT","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":410,"txt":"Вона","rate":0.0200958,"cc":"KRW","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":484,"txt":"Мексіканський песо","rate":1.226917,"cc":"MXN","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":498,"txt":"Молдовський лей","rate":1.35804,"cc":"MDL","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":554,"txt":"Новозеландський долар","rate":15.145245,"cc":"NZD","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":578,"txt":"Норвезька крона","rate":2.655897,"cc":"NOK","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":643,"txt":"Російський рубль","rate":0.37387,"cc":"RUB","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":682,"txt":"Саудівський рiял","rate":6.422082,"cc":"SAR","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":702,"txt":"Сінгапурський долар","rate":17.4401,"cc":"SGD","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":710,"txt":"Ренд","rate":1.59767,"cc":"ZAR","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":752,"txt":"Шведська крона","rate":2.460943,"cc":"SEK","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":756,"txt":"Швейцарський франк","rate":24.249127,"cc":"CHF","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":818,"txt":"Єгипетський фунт","rate":1.481308,"cc":"EGP","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":826,"txt":"Фунт стерлінгів","rate":29.663376,"cc":"GBP","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":840,"txt":"Долар США","rate":24.082809,"cc":"USD","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":933,"txt":"Бiлоруський рубль","rate":11.61009,"cc":"BYN","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":944,"txt":"Азербайджанський манат","rate":14.166358,"cc":"AZN","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":946,"txt":"Румунський лей","rate":5.542366,"cc":"RON","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":949,"txt":"Турецька ліра","rate":4.254714,"cc":"TRY","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":960,"txt":"СПЗ(спеціальні права запозичення)","rate":32.854994,"cc":"XDR","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":975,"txt":"Болгарський лев","rate":13.464849,"cc":"BGN","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":978,"txt":"Євро","rate":26.334552,"cc":"EUR","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":985,"txt":"Злотий","rate":6.003545,"cc":"PLN","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":12,"txt":"Алжирський динар","rate":0.209864,"cc":"DZD","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":50,"txt":"Така","rate":0.297599,"cc":"BDT","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":51,"txt":"Вiрменський драм","rate":0.05281267,"cc":"AMD","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":368,"txt":"Іракський динар","rate":0.021193,"cc":"IQD","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":417,"txt":"Сом","rate":0.360197,"cc":"KGS","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":422,"txt":"Ліванський фунт","rate":0.0167182,"cc":"LBP","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":434,"txt":"Лівійський динар","rate":17.875808,"cc":"LYD","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":458,"txt":"Малайзійський ринггіт","rate":5.97885,"cc":"MYR","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":504,"txt":"Марокканський дирхам","rate":2.617106,"cc":"MAD","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":586,"txt":"Пакистанська рупія","rate":0.160154,"cc":"PKR","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":704,"txt":"Донг","rate":0.00108403,"cc":"VND","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":764,"txt":"Бат","rate":0.822096,"cc":"THB","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":784,"txt":"Дирхам ОАЕ","rate":6.846033,"cc":"AED","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":788,"txt":"Туніський динар","rate":8.775385,"cc":"TND","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":860,"txt":"Узбецький сум","rate":0.002682,"cc":"UZS","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":901,"txt":"Новий тайванський долар","rate":0.803147,"cc":"TWD","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":934,"txt":"Туркменський новий манат","rate":7.184032,"cc":"TMT","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":936,"txt":"Ганських седі","rate":4.613598,"cc":"GHS","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":941,"txt":"Сербський динар","rate":0.236784,"cc":"RSD","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":972,"txt":"Сомонi","rate":2.596996,"cc":"TJS","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":981,"txt":"Ларi","rate":8.53587,"cc":"GEL","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":959,"txt":"Золото","rate":36031.495,"cc":"XAU","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":961,"txt":"Срiбло","rate":432.768,"cc":"XAG","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":962,"txt":"Платина","rate":22445.178,"cc":"XPT","exchangedate":"30.09.2019"
//     }
//     ,{
//         "r030":964,"txt":"Паладiй","rate":40049.711,"cc":"XPD","exchangedate":"30.09.2019"
//     }
// ]
//
// function processingRates() {
//     // let newRates = currencies.sort((a,b) => a['rate'] - b['rate'])
//     // document.write('<ul>');
//     //
//     // for (let i of newRates){
//     //     document.write(`<ol>${i['cc']} - ${i['rate']}</ol>`)
//     // }
//
//     // [2,3,4,5,6, 7] >20 <20
//     // 'валюта у которой курс  больше 20 - отсортировать в порядке возрастания. Кавлюта у которой курс меньше' +
//     // ' - 20 в порядке убывания'
//     // document.write('</ul>')
//     let arr1 = [];
//     let arr2 = [];
//     currencies.forEach(item => {
//         if (item.rate > 20) {
//             arr1.push(item);
//         } else {
//             arr2.push(item);
//         }
//     })
//     let sortArr1 = arr1.sort((a, b) => a['rate'] - b['rate']);
//     let sortArr2 = arr2.sort((a, b) => b['rate'] - a['rate']);
//     let ResultArr = [...sortArr1, '<hr>', ...sortArr2];
//     return ResultArr;
// }
//
// let result = processingRates();
// result.forEach(item => {
//     if (item.txt) {
//         document.write(`${item.txt} - ${item.rate} <br>`);
//     } else {
//         document.write (item);
//     }
//
//
// })

const set = new Set();
set.add(1);
// set.set(2);
// set.set(2);
// set.set(3);
// set.set(3);
// set.set(4);
// set.set(5);
// console.log(set);
// // let myData = {value: 'password', [Simbol.iterator]: function() {
// //
// //     }}
// const map = new Map();
// map.set(myData);
// map.set({value: true}, true);
// console.log(map);

// let arr = [{name: 'Ivan'}, {name: 'Anna'}];
//
// let weakMap = new WeakMap();
// weakMap.set(arr[0], 'password');
// weakMap.set(arr[1], 'password2');

