// /*ЗАДАЧА 0
// * Написать функцию customCharAt(string,index)
// * string - исходная строка
// * index - индекс под которым в строке лежит нужный символ
// * ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ - символ в указанной строке под указанным индексом
// * */
//
// /*ЗАДАЧА 1
// * Получить от пользователя строку и перевести все ее ПАРНЫЕ символы в верхний регистр
// */
//
// /* ЗАДАЧА 2
// * Написать функцию cutMexLength(string, maxlength)
// * которая принимает исходную строку и значение максимальной длинны
// * ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ
// * 	если символов в строке больше, чем maxLength, то выместо всего что ПОСЛЕ нужно вставить '...'
// * 	если символов в строке меньше или столько же, сколько указано в maxLength, отдаем ее без изменений
// */
//
// /* ЗАДАЧА 3
// * Написать функцию getDayAgo(numberOfDays)
// * Она должна вернуть название дня недели, который был numberOfDays дней назад.
// */
// const days = [
//     'Sunday',
//     'Monday',
//     'Tuesday',
//     'Wednesday',
//     'Thursday',
//     'Friday',
//     'Saturday'
// ];
//
// /*ЗАДАЧА 4
// * Написать функцию, которая принимает аргументом строку с датой в формате "ДД/ММ/ГГГГ"
// * ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ: день недели первого дня месяца
// * Написать такую же функцию для получения дня недели последнего дня месяца.
// * */
//
// /* ЗАДАЧА 5
// * Есть объект склада,
// * Написать метод объекта getAmount(goodsList), который принимает в качестве аргумента СТРОКУ СО СПИСКОМ ТОВАРОВ,
// * ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ - строка в которой содержится для каждого товара - имя и оставшееся количество на складе.
// *
// * В случае если какого-то товара на складе нет в принципе - напротив него написать 'not found'.
// *
// * Названия товаров не должны быть чувствительны к регистру, т.е. если на складе есть apple,
// * то пользователь может ввести Apple или aPPle и получить нужную ему инфомрацию.
// *
// * 	ПРОДВИНУТАЯ СЛОЖНОСТЬ:
// *			добавить разделение по категориям. Продукты питания засунуть в массив под ключем food. Это теперь будет массив с объектами,
// *			каждый из которых имеет два ключа name - имя и amount - наличие (число показывающее количество штук). по такому же принципу
// *			создать еще хотябы 2-3 категории по типу "бытовая техника", "бытовая химия". И, соответственно, переписать метод getAmount следующим образом:
// *				Добавить второй НЕОБЯБАЗТЕЛЬНЫЙ агрумент category. Если он введен, то искать товары сразу в нужной категории,
// *				если же второй агрумент не введен, то искать во всех категориях по очереди.
// */
// const Storage = {
//     apple: 8,
//     beef: 162,
//     banana: 14,
//     chocolate: 0,
//     milk: 2,
//     water: 16
//     coffee: 0,
//     blackTea: 13,
//     cheese: 0
// }
//
// const x = '';
// const y = "";
// const z = ``;
//
// const x = 'Hello I`m Ivan';
//
// const aa  = x+y;
// const bb = `${x}${y}`;
//
//
// const pp = 'Ivan';
// toLowerCase();
// pp.toUpperCase();
// pp.charAt(2);
//
// pp.indexOf('a');
//
// pp.slice(1,-1);
// pp.substr(1,2);
// pp.substring(1, -1);
// pp.lastIndexOf();
// pp.includes('needTOSerach');
// pp.startsWith();
// pp.endsWith();
//
// pp.trim('   dsfsdfsdfsd    sdfsd  s s df sdfd  sd fs d      ');
//
// const x = pp.repeat(3);
// const pp = 'Hello world';
// const truncated = truncate(pp, 3);
// console.log(truncated);

// [a-zA-Z0-9]{2}\.@-[a-z]{2-8}.[a-z]{2-4}
// const newStr = 'Hello world from me';
// const arr= newStr.split('');


// let word ="Don't repeat yourself";
// word = word.toUpperCase();

// const newWord = word.toUpperCase();
// alert(newWord);
// alert(word);

// let o = {
//     name: 'Ivan',
// }
//


// function getAbr(a){
//     let abrArr=a.split(' ');
//     let abrArrayStr='';
//     let newData;
//     for (x of abrArr) {
//         let bigLet=(x.toUpperCase());
//         let index =bigLet[0];
//         // abrArrayStr+=index;
//         newData = abrArrayStr.concat(index);
//         console.log(newData);
//
//     }
//     return(newData);
//
// }
// console.log(getAbr(word));

// let str = 'hyper text markup language';
//
// function getCaps(word) {
//     let wordArr = word.split(' ');
//     let newArr = [];
//     for (let w of wordArr) {
//         let capWord = w[0].toUpperCase() + w.slice(1);
//         newArr.push(capWord);
//     }
//     return newArr.join(' ');
// }
// console.log(getCaps(str));


// * Написать функцию customCharAt(string,index)
// * string - исходная строка
// * index - индекс под которым в строке лежит нужный символ
// * ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ - символ в указанной строке под указанным индексом
// * */

// let customCharAt = (word,i) => {
//      return word.charAt(i);
// };
//
// console.log(customCharAt('Hello world', 3));

// /*ЗАДАЧА 1
// * Получить от пользователя строку и перевести все ее ПАРНЫЕ символы в верхний регистр
// */
//
// let getStr = function (str) {
//    let getArr = str.split('');
//    for (let i = 0; i< getArr.length;i++){
//        if (i%2 == 0) {
//            getArr[i] = getArr[i].toUpperCase()
//        }
//    }
//
//    let result = '';
//    for (let i=0; i < getArr.length; i++){
//        result += getArr[i];
//    }
//     return result;
// } ;
// console.log(getStr('Hello world'));

// /* ЗАДАЧА 2
// * Написать функцию cutMexLength(string, maxlength)
// * которая принимает исходную строку и значение максимальной длинны
// * ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ
// * 	если символов в строке больше, чем maxLength, то выместо всего что ПОСЛЕ нужно вставить '...'
// * 	если символов в строке меньше или столько же, сколько указано в maxLength, отдаем ее без изменений
// */

// const cutMaxLength = (str, maxLength) => {
//   if (str.length > maxLength){
//       str = str.slice(0,maxLength) + '...';
//   }
//
//   return str;
// };
//
// let check = cutMaxLength('Hello world', 24);
//
// console.log('result ' + check);

// const newDate = new Date().setFullYear(2020);//1601283904287
// console.log(newDate);

// const newSecDate = new Date(newDate);
// console.log(newSecDate.getFullYear());
// const today = new Date();
// console.log(today.getMonth());
// console.log(today.getDay());
// console.log(today.getDate());

const days = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday'
];


// Написать функцию getDay(numberOfDays)
function getDay(data) {
    let day = data.getDay();
    return days[day];
}
// console.log(getDay(new Date(2019,8,1)));


// Написать функцию getDayAgo(numberOfDays)
function someDate(numberOfDays) {
    const currentDate = new Date();
    const currentDay = currentDate.getDate();
    const enterDate = currentDate.setDate(currentDay-numberOfDays);
    const neededDate = new Date (enterDate);

    return getDay(neededDate);
}

// let res = someDate(12);



const Storage = {
    apple: 8,
    beef: 162,
    banana: 14,
    chocolate: 0,
    milk: 2,
    water: 16,
    coffee: 0,
    blackTea: 13,
    cheese: 0
}

function getProduct(product) {
    // let searchableProduct = Storage[product.toLowerCase()];
    // if (searchableProduct === 0){
    //     return "No product in storage";
    // }
    // else {
    //     return searchableProduct;
    // }
    return (Storage[product.toLowerCase()]) ? Storage[product.toLowerCase()] : "No product in storage";

}
let result = getProduct("somth");
console.log(result);



