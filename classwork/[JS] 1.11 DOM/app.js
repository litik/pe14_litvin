/* ЗАДАНИЕ - 1:
* Создать серый квадрат , размер которого укажет пользователь.
* - спрашиваем размер в пикселях (одна цифра)
* - создаем елемент div
* - задаем ему стили "пакетом"
* - вставляем его на страницу (в ДОМ дерево)*/
// let askUser;
// let askUser2;
// do{
// askUser = +prompt('enter your size','askUser');
// askUser2 = +prompt('enter your size','');
// } while(
//     isNaN(askUser) ||
//     isNaN(askUser2) ||
//     askUser===0 ||
//     askUser2===0
//     )
//
// const kv=document.createElement('div')
// kv.style.cssText =`width:${askUser}px;
//            height:${askUser2}px;
//            background:gray;`
// document.body.append(kv);

/* ЗАДАНИЕ - 2
* Создать ДВА квадрата, одного размера, но разных цветов.
* Пользователь указывает один раз размер(одна цифра).
* затем указывает цвет для первого квадрата, потом цвет для второго квадрата.
* - спрашиваем размер в пикселях (одна цифра)
* - спрашиваем цвет-1
* - спрашиваем цвет-2
* - создаем два елемента div
* - задаем каждому стили "пакетом"
* - вставляем оба елемента на страницу (в ДОМ дерево)*/
// let square1=document.createElement('div');
// let square2=document.createElement('div');
// let size=prompt('Enter size');
// let color1=prompt('Enter color');
// let color2=prompt('Enter color');
// square1.style.cssText=`width:${size}px;
//                        height:${size}px;
//                         background:${color1}`;
// square2.style.cssText=`width:${size}px;
//                        height:${size}px;
//                         background:${color2}`;
// document.body.append(square2,square1);

/* ЗАДАНИЕ - 3
* Переделать задание 2 следующим образом:
* Спрашивать у пользователя количество прямоугольников которые нужно вывести на экран, цвет которым они все будут закрашены.
* - спрашиваем количество прямоугольников
* - спрашиваем цвет для заливки
* - в цикле на каждой итерации:
	* -- создаем елемент div
	* -- ему которому присваиваем CSS КЛАСС с базовыми свойствами (размер всех граней 150px, отступ одинаковый со всех сторон 5px)
	* -- добавляем елемент на страницу (в ДОМ дерево)
*/

// let amount=+prompt('Enter amount');
// let color=prompt('Enter color');
// // square.style.cssText=`background:${color}`;
// // square.classList.add('element');
// for (let i = 0; i < amount; i++) {
//     let square=document.createElement('div');
//     document.body.append(square);
//     square.style.cssText=`background:${color}`;
//     square.classList.add('element');
// }

/* ЗАДАНИЕ - 4:
* Создать "шахматную" доску, размеры (количество квардратов размером 150px) и гамму которой указывает пользователь.
* - спрашиваем размер (одна цифра)
* - спрашиваем цвет-1 - для "белых" ячеек
* - спрашиваем цвет-2 - для "черных" ячеек
* - создаем фрагмент документа
* - в цикле - нужное количество раз создаем елементы нужного цвета и добавляем их вовнутрь фрагмента. БАЗОВЫЕ СТИЛИ ПРИСВАИВАЕМ ЧЕРЕЗ КЛАСС (базовые стили те же что и в задании 3)
* - вставляем фрагмент на страницу (в ДОМ дерево)*/

function getChess() {
    let size = +prompt("Desk size?");

    while (!isFinite(parseInt(size))){
        console.log({error: `${size} - is not a number`});
        size = +prompt('Another try?')
    }

    const fra = new DocumentFragment();

    let outerArr = new Array(size);
    console.log(outerArr);
    for (let x = 0; x<outerArr.length; x++) {
        outerArr[x] = new Array(size);
    }

    for (var i = 0; i< outerArr.length; i++) {
        for(var j = 0 ; j< outerArr[i].length; j++) {
            let div = document.createElement('div');
            div.classList.add('element');
            div.style.cssFloat = 'left';
            if ((i+j) % 2 === 0) {
                div.style.background = 'red';
            } else {
                div.style.background = 'blue';
            }

            if ((j === 0)) div.style.clear = 'both';
            outerArr[i][j] = div;
        }
    }

    for (var a = 0; a< outerArr.length; a++) {
        for(var b = 0 ; b< outerArr[a].length; b++) {
            fra.append(outerArr[a][b]);
        }
    }
    document.body.append(fra)

}
getChess();

/* ЗАДАНИЕ - 5:
* добавить в реализацию расстановку фигур.
* Из решения задания 3 убираем код отвечающий за размеры доски. Оставляем ее стандартным 800рх на 800 рх.
* По прежнему спрашиваем цвет для белых и черных.
* На этапе создания и добавления ячеек в первых двух рядах каждой ячейке дочерним елементом добавляем ромб любого контрастного с фоном цвета.
* в последних двух рядах - каждой ячейке добавляем дочерним елементом круг любого контрастного с фоном цвета
*/

// // const x= document.getElementById('div');
// const ourUl = document.querySelector('ul');
// const li = document.createElement('li');
// // li.innerText = '<li>1</li>';
// li.innerHTML = 'first';
// ourUl.prepend(li);

// ourUl.insertAdjacentElement('afterbegin', li);
// ourUl.insertAdjacentText('afterbegin', '<li>first</li>');

// ourUl.insertAdjacentHTML('afterbegin', '<li>first</li>')



// ourUl.insertAdjacentElement('afterbegin', li); //ourUl.prepend(li);

// ourUl.insertAdjacentElement('afterend', li); //after();
// ourUl.insertAdjacentElement('beforebegin', li); //before();
// ourUl.insertAdjacentElement('beforeend', li); // append();

// ourUl.prepend(li);
// ourUl.remove();
// const secondLi = document.querySelector('ul > li');
// // secondLi.remove();
//
// const p1 = document.querySelector('div > p:first-child');
// const section = document.createElement('section');
// section.innerHTML= 'section';
// p1.replaceWith(section);
//
// const p2 = document.querySelector('div > p');
// const cloneWithp2 = p2.cloneNode(true);
// // console.log(cloneWithp2 === p2);
// secondLi.prepend(cloneWithp2);
// p2.after(secondLi);



// const div = document.createElement('div');
// div.innerText = 'Hello i`m div';
// document.body.append(div);
// const div1 = document.createElement('div');
//
// let i = 1;
// i++;
// i +=1;

// let div;
// const fragment = new DocumentFragment();
// for (let i = 0 ; i < 100; i++) {
//     div = document.createElement('div');
//     div.innerHTML = i;
//     fragment.append(div);
    // document.body.append(div)
    // document.body.append(fragment);
// }

// document.body.append(fragment)

// console.log(div);












