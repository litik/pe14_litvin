/* ЗАДАНИЕ 1
*Вывести в консоль при помощи метода console.log() следующее:
  - Максимальное положительное число
  - Минимальное отрицательное число
  - НЕ число
  - Максимальное допустимое значение в JavaScript
  - Минимальное допустимое значение в JavaScript
* */

// console.log('Максимальное положительное число - ',Number.MAX_VALUE);
// console.log('Минимальное отрицательное число -'
// ,Number.MIN_VALUE);
// console.log('НЕ число -',NaN);
// console.log('Максимальное допустимое значение в JavaScript -', Number.MAX_SAFE_INTEGER);
// console.log('Минимальное допустимое значение в JavaScript -',Number.MIN_SAFE_INTEGER);

/* ЗАДАНИЕ 2
* Создать две переменных, в первой - любое число, во второй - строка, в которой содержится любое слово
* Вывести в консоль СУММУ двух новосозданных переменных
* Вывести в консоль РАЗНИЦУ двух новосозданных переменных
* Вывести в консоль ПРОИЗВЕДЕНИЕ двух новосозданных переменных
*
* Объяснить результат каждой операции
* */
// let num = 13;
// let str="5";
//
// console.log("СУММУ->", num + str);
// console.log("РАЗНИЦУ->", num - str);
// console.log("ПРОИЗВЕДЕНИЕ->", num * str);
// console.log("ПРОИЗВЕДЕНИЕ->", num % str);
// console.log('а парное ли число записано в переменную num? -> ', num % 2 === 0);


/* ЗАДАНИЕ 3
* Создать две переменных. Одной присвоить - true, второй - false
* Вывести в консоль следующее:
*   - результат свавнения 0 и переменной в которую записано false, с использованием оператора ==
*   - результат свавнения 1 и переменной в которую записано true, с использованием оператора ===
* */

// let t = true;
// let f = false;
// console.log('0 == f ->', 0 == f);
// console.log('0 == f ->', 1 === t);


/* ЗАДАНИЕ 4
* Вывести с помощью JS в консоль следующие задачи и рассказать как эти операции были выполнены.
* Переменные x = 6, y = 15, z = 4:
    * x += y  - x++ * z
    * z = --x -y * 5
    * y /= x + 5 % z
* 'random string' + 500
* 'random string' + +'number'
* 'random string' + +'500n'
* 'random string' + parseInt('404not found')
* !!'false' == !!'true'
* 'true' == true
* 'true' === true
* NaN == 1` `NaN == NaN` `NaN === NaN` `NaN > NaN` `NaN < NaN` `NaN >= NaN` `NaN <= NaN
* [] == true` `{} == true
* */
//  let x = 6, //-4
//      y = 15,
//      z = 4; //-79
//
// console.log('x += y  - x++ * z ', x += y - x++ * z);
// console.log('z = --x -y * 5', z = --x - y * 5);
// console.log('y /= x + 5 % z', y /= x + 5 % z);
// console.log('\'random string\' + 500', 'random string' + 500);
// console.log('\'random string\' + +\'number\'', 'random string' + +'number');
// console.log('\'random string\' + +\'500n\'', 'random string' + +'500n');
// console.log('\'random string\' + parseInt(\'404not found\')', 'random string' + parseInt('404not found'));
// console.log('!!\'false\' == !!\'true\'', !!'false' == !!'true');
// console.log('\'true\' == true', 'true' == true);
// console.log('\'true\' === true', 'true' === true);

/* ЗАДАНИЕ 5
* С помощью модальных окон получить от пользователя 3 числа и вывести в консоль:
*   - среднее арифметическое
*   - максимальное число
*   - минимальное число
* */

let num1 = prompt("Enter a number: ");
let num2 = prompt("Enter a number: ");
let num3 = prompt("Enter a number: ");

let average = (+num1 + +num2 + +num3) / 3;
console.log(average);

console.log(Math.max(num1, num2, num3));
console.log(Math.min(num1, num2, num3));


















