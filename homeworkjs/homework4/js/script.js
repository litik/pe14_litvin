let createNewUser = () => {
    let newUser = {};

    Object.defineProperty( newUser, "firstName", {
        value: prompt("Please Enter first name"),
        writable: false
    });

    Object.defineProperty( newUser, "lastName", {
        value: prompt("Please Enter last name"),
        writable: false
    });

    newUser.getLogin = function(){
        return (this.firstName.charAt(0) + this.lastName).toLowerCase();
    };
    return newUser;
};

let newUser;

newUser = createNewUser();
console.log('newUser.firstName = "' + newUser.firstName + '"');
console.log('newUser.lastName = "' + newUser.lastName + '"');
console.log('newUser.getLogin = "' + newUser.getLogin() + '"');
