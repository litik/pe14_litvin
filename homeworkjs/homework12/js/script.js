let buttonStop = document.createElement("button"),
    buttonContinue = document.createElement("button"),
    images = ['img/1.jpg', 'img/2.jpg', 'img/3.JPG', 'img/4.png'],
    length = images.length,
    index  = 1;

buttonStop.innerHTML = "Прекратить";
buttonContinue.innerHTML =  "Возобновить показ";

buttonStop.classList.add("button");
buttonContinue.classList.add("button");

document. querySelector("script").before(buttonStop);
buttonStop.before(buttonContinue);

buttonContinue.onclick = function (event) {
    buttonContinue.disabled = true;
    buttonStop.disabled = false;
    let interval = setInterval(function() {
        if(index === length) {
            index = 0;
        }
        document.getElementById('image_id').src = images[index++];
    }, 10000);
    buttonStop.onclick = function (event) {
        buttonContinue.disabled = false;
        buttonStop.disabled = true;
        clearInterval(interval);
    };
};
buttonContinue.onclick();

// let delay = 10;
// let counter = document.getElementById('counter');
// let timer = setInterval(function() {
//     counter.innerHTML = --delay;
//     if (!delay) {
//         clearInterval(timer);
//         document.querySelector("body").style.display='none';
//     }
// }, 1000);
// counter.innerHTML = delay;
