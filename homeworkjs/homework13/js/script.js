document.addEventListener('DOMContentLoaded', onReady);

function onReady() {


    const btnChange = document.getElementsByClassName('buttonPress')[0],
        body = document.body;


    let blockIsActive = localStorage.getItem("blockIsActive");
    if (blockIsActive === "true") {
        body.style.backgroundColor ='white';
    }


    btnChange.addEventListener('click', onChangeColorThemesClick);


    function onChangeColorThemesClick() {


        localStorage.setItem("blockIsActive", "true");
        const bodyLocal = getComputedStyle(body).backgroundColor;

        if (!localStorage.getItem('bodyLocal')) {

            localStorage.setItem('bodyLocal', bodyLocal);

            let localBody = localStorage.getItem('bodyLocal');


            if (localBody === bodyLocal) {
                body.style.backgroundColor = '#000000';
            }

        } else {
            body.style.backgroundColor = localStorage.getItem('bodyLocal');
            localStorage.clear();
        }

    }

}