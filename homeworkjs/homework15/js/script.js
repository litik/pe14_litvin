$(function () {

    const $menu = $('.top_menu'),
        $page = $('html, body'),
        buttonTop = $('<button>').attr({'type': 'button', 'class': 'button-top'});

    $(buttonTop).text('Наверх')
        .hide()
        .on('click', function () {
            $page.animate({
                scrollTop: 0
            }, 3000);
        });

    $('header').append($(buttonTop));

    $(window).scroll(function () {
        if ($(this).scrollTop() > $(window).height()) {
            $(buttonTop).show();
        } else {
            $(buttonTop).hide();
        }
    });


    $($menu).on('click', function (e) {
        let thisElem = $(this);

        $page.animate({
            scrollTop: $(thisElem.attr('href')).offset().top

        }, 3000);

        e.preventDefault();
    });

    const btnToggle = $('.button-toggle');

    btnToggle.on('click', function () {
        $('.parallax2').slideToggle();
    })


});