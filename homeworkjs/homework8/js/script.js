let field = document.getElementById("input");
let span = document.createElement("span");
let button = document.createElement("button");
let span2 = document.createElement("span");

field.onfocus = function() {
    field.style.border = `3px solid green`;
    span2.innerHTML = "";
};

field.onblur = function() {
    if( this.value < 0 ) {
        field.style.border = `3px solid red`;
        field.cssText = "text";
        span.innerHTML = "";
        button.remove();
        errorMessage();
    } else {
        field.style.border = ``;
        field.cssText = "color-text-green";
        correctMessage();
    }
};

function correctMessage () {
    span.innerHTML = `Текущая цена: ${field.value}`;
    document.getElementById("form").before(span);


    button.innerText = "x";
    button.style.display = "inline-block";
    button.style.borderRadius = "50%";
    span.after(button);

    button.addEventListener("click", (event) => {
        span.innerHTML = "";
        button.remove();
        field.value = "";
    });
}

function errorMessage () {
    span2.innerText = "Please enter correct price";
    document.getElementById("form").after(span2);

}
